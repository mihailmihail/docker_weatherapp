#!/bin/bash

if [ "$1" == "build" ]; then
  docker image build -t weatherapp_frontend ./frontend
  docker image build -t weatherapp_backend ./backend
fi

kubectl run weatherfrontend --image=weatherapp_frontend --image-pull-policy=Never --env "ENDPOINT=http://localhost:8080/api"
kubectl run weatherbackend --image=weatherapp_backend --image-pull-policy=Never  --env "APPID=67e1c453cdddaaaafa68ad6e8d471e43" --env "MAP_ENDPOINT=http://api.openweathermap.org/data/2.5" --env "TARGET_CITY=Helsinki" --env "PORT=3001"

kubectl expose deployment weatherfrontend --type=NodePort --port 3000
kubectl expose deployment weatherbackend --type=NodePort --port 3001

nohup kubectl port-forward service/weatherfrontend --address 0.0.0.0 8090:3000 > /dev/null 2>&1 &
nohup kubectl port-forward service/weatherbackend --address 0.0.0.0 8080:3001 > /dev/null 2>&1 &
