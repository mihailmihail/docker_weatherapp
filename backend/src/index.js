const debug = require('debug')('weathermap');

const Koa = require('koa');
const router = require('koa-router')();
const fetch = require('node-fetch');
const cors = require('kcors');

const appId = process.env.APPID || '';
const mapURI = process.env.MAP_ENDPOINT || "http://api.openweathermap.org/data/2.5";
const targetCity = process.env.TARGET_CITY || "Helsinki,fi";

const port = process.env.PORT || 9001;

const app = new Koa();

app.use(cors());

const fetchWeather = async () => {
  const endpoint = `${mapURI}/weather?q=${targetCity}&appid=${appId}&`;
  const response = await fetch(endpoint);

  return response ? response.json() : {}
};

const parseWeather = (weatherData) => {
        return {
           data: weatherData.weather ? weatherData.weather[0] : {},
           coords: {
        lon: weatherData.coord.lon ? weatherData.coord.lon : 0,
        lat: weatherData.coord.lat ? weatherData.coord.lat : 0,
       },
       temp: {
        actual: weatherData.main.temp ? weatherData.main.temp : 0,
        feelsLike: weatherData.main.feels_like ? weatherData.main.feels_like : 0,
           }
    }
};

router.get('/api/weather', async ctx => {
  const weatherData = await fetchWeather();

  ctx.type = 'application/json; charset=utf-8';
  ctx.body = weatherData.weather ? parseWeather(weatherData) : {};
});


app.use(router.routes());
app.use(router.allowedMethods());

app.listen(port);

console.log(`App listening on port ${port}`);
