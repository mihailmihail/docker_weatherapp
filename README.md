# Weatherapp

There was a beautiful idea of building an app that would show the upcoming weather. The developers wrote a nice backend and a frontend following the latest principles and - to be honest - bells and whistles. However, the developers did not remember to add any information about the infrastructure or even setup instructions in the source code.

Luckily we now have [docker compose](https://docs.docker.com/compose/) saving us from installing the tools on our computer, and making sure the app looks (and is) the same in development and in production. All we need is someone to add the few missing files!

## Address

App @ localhost:8090

## How to run

### Docker-compose
Run 'docker-compose up --build' in root folder
### Minikube
Run 'start.sh build'
Incase you need to remove the containers run 'deleteKubeContainers.sh'
## Additions

Added:
 - temperature ( in celsius )
 - latitude, longitude


