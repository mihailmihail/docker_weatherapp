#!/bin/sh

kubectl delete -n default deployment weatherbackend
kubectl delete -n default deployment weatherfrontend

kubectl delete -n default service weatherbackend
kubectl delete -n default service weatherfrontend
