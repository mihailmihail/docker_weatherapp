import React from 'react';
import ReactDOM from 'react-dom';

const baseURL = process.env.ENDPOINT

const getWeatherFromApi = async () => {
  try {
    const response = await fetch(`${baseURL}/weather`);
    return response.json();
  } catch (error) {
    console.error(error);
  }

  return {};
};

const parseCelcius = (temperature) => {
    return Math.round((temperature - 273.15) * 100) / 100;
};

class Weather extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      icon: "",
      lat: 0.0,
      long: 0.0,
      temp: 0.0,
    };
  }



  async componentWillMount() {
    const weather = await getWeatherFromApi();
    console.log(weather)
    this.setState({
        icon: weather.data.icon.slice(0, -1),
        lat: weather.coords.lat,
        lon: weather.coords.lon,
        temp: parseCelcius(weather.temp.actual),
    });
  }

  render() {
    const { icon, lat, lon, temp } = this.state;

    return (
      <div className="icon">
        { icon && <img src={`/img/${icon}.svg`} /> }
        <div className="text">
            <p> Current temperature: {temp}  &#8451; </p>
            <p> Latitude: {lat} </p>
            <p> Longitude: {lon} </p>
        </div>
      </div>
    );
  }
}

ReactDOM.render(
  <Weather />,
  document.getElementById('app')
);
